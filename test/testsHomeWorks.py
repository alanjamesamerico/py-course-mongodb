'''
Created on 23 de ago de 2016

@author: Alan James
'''

from pymongo import MongoClient
from pip._vendor.html5lib.treewalkers import pprint
conn = MongoClient('localhost',27017)
db = conn.blog

documents = db.posts.find().limit(3)

for d in documents:
    print(d)

'''
permalink   = "Post_Comment_4"
comment     = {'author': "Thais", 'body': "buzz"}

query   = {"permalink": permalink}
change  = {"$push": {"comments": comment}}

up = db.posts.update_one(query, change)

print("up: %s\nType %s" %(up, type(up)))
print ("\nFim")
'''
