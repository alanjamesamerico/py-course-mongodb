from pymongo import MongoClient
import pymongo


conn = MongoClient('localhost', 27017)
db = conn.students

query = {"type":{"$eq":"homework"}}
grades = db.grades.find(query)
grades = grades.sort([("student_id", pymongo.ASCENDING),("score", pymongo.ASCENDING)])

rGrades = db.grades
elementos = range(0, grades.count())
cont = 0
removed = 0

for i in elementos:
    if (elementos[i] < grades.count()-1):
        a = grades[i]['student_id']
        b = grades[i+1]['student_id']
    else:
        pass  
    if (a == b and (elementos[i] < grades.count()-1)):
        if(grades[i+1]['score'] > grades[i]['score']):
            print("Documento removido -> score: %d\nMaior: %d" %(grades[i]['score'],grades[i+1]['score']))
            rGrades.remove({"score":grades[i]['score']})
            removed += 1
    else:
        pass
    
print("\n\n\tcont: %d\n\n\tremoved: %d" %(cont, removed))